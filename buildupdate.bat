@ECHO OFF
GOTO START

REM ===================
REM Entry Point
REM ===================
:START

REM CALL "bin\resultenv.bat"

IF NOT DEFINED WYBUILDDIR (
	ECHO Error - WYBUILDDIR not defined
	GOTO :EOF
)
IF NOT DEFINED STRVERSION (
	ECHO Error - STRVERSION not defined
	GOTO :EOF
)
IF NOT DEFINED RELEASE (
	ECHO Error - RELEASE not defined
	GOTO :EOF
)
IF NOT DEFINED RELEASEBASE (
	ECHO Error - RELEASEBASE not defined
	GOTO :EOF
)

:: --------------------
:COPYFILE
:: --------------------
RMDIR /S /Q "%RELEASEBASE%\%RELEASE%\"
ECHO Copying files to release %RELEASEBASE%\%RELEASE%\ ...
unzip bin\%RELEASE%.zip -d %RELEASEBASE%\%RELEASE%
IF %ERRORLEVEL% NEQ 0 (
	ECHO Error - Can not find nebula-build artifact
	EXIT /B %ERRORLEVEL%
)
:: --------------------
:PREPAREXML
:: --------------------
COPY /Y "addversion.template.xml" "bin\addversion.xml"
fnr --cl --dir "%CD%\bin" --fileMask "addversion.xml" --find "$(STRVERSION)" --replace "%STRVERSION%"
fnr --cl --dir "%CD%\bin" --fileMask "addversion.xml" --find "$(RELEASEBASE)" --replace "%RELEASEBASE%"
fnr --cl --dir "%CD%\bin" --fileMask "addversion.xml" --find "$(RELEASE)" --replace "%RELEASE%"

:: --------------------
:BUILDUPDATE
:: --------------------
RMDIR /S /Q "Updates\"
RMDIR /S /Q "wyUpdate\"
%WYBUILDDIR%\wybuild.cmd.exe "nebula.wyp" /bwu -add="bin\addversion.xml"
IF %ERRORLEVEL% NEQ 0 (
	ECHO Error - Failed to build wyUpdate
	EXIT /B %ERRORLEVEL%
)
XCOPY /Y "wyUpdate\*" "%RELEASEBASE%\%RELEASE%\"
%WYBUILDDIR%\wybuild.cmd.exe "nebula.wyp" /bu
IF %ERRORLEVEL% NEQ 0 (
	ECHO Error - Failed to build patch updates
	EXIT /B %ERRORLEVEL%
)

:: --------------------
:COMMIT
:: --------------------
REM "%GIT_HOME%\git" add .
"%GIT_HOME%\git" commit -am "Automatic release %STRVERSION%"

:: --------------------
:UPLOAD
:: --------------------
zip -j bin\%RELEASE%.zip wyUpdate\*
RENAME "%CD%\bin\%RELEASE%.zip" "%RELEASE%.full.zip"
zip -r bin\%RELEASE%.patch.zip Updates\* wyUpdate\*

:: --------------------
:END
:: --------------------